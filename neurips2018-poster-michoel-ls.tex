\documentclass[final,12pt]{a0poster}

%\usepackage[sfdefault]{ClearSans}
% \usepackage{helvet}
% \renewcommand{\familydefault}{\sfdefault}
% \usepackage{sansmath} 
% \sansmath

\usepackage[sfdefault,scaled=1.]{FiraSans}
\usepackage[T1]{fontenc}
\usepackage{textcomp}
\usepackage[varqu,varl]{zi4}% inconsolata typewriter
\usepackage{amsfonts,amsmath,mathbbol,bm,amssymb} 
\usepackage{newtxsf}

\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage[bottom=0pt,left=0mm,right=0mm,top=0mm]{geometry}

\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{tikz}
\usepackage{wasysym}
\usepackage{multicol}
%\usepackage{url}
\usepackage{booktabs}
\usepackage[hidelinks]{hyperref}

\setlength{\columnseprule}{1pt}
\usetikzlibrary{arrows,shapes.misc}

\newcommand{\R}{\mathbb{R}}
\newcommand{\C}{\mathbb{C}}
\newcommand{\U}{\mathbb{1}}
\newcommand{\Rj}{\mathcal{R}}
\newcommand{\N}{\mathcal{N}}
\newcommand{\Mi}{\mathcal{I}}
\newcommand{\I}{\mathbb{1}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\D}{\mathcal{D}}
\renewcommand{\I}{\mathcal{I}}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\Imax}{I_{\max}}
\newcommand{\E}{\mathbb{E}}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\nw}{^{\text{(new)}}}
\newcommand{\od}{^{\text{(old)}}}

\newcommand{\uh}{\hat u}
\newcommand{\uhj}{\hat u_j}
\newcommand{\ut}{\hat u_\tau}
\newcommand{\utj}{\hat u_{\tau,j}}

\newcommand{\xh}{\hat x}
\newcommand{\xhj}{\hat x_j}
\newcommand{\xt}{\hat x_\tau}
\newcommand{\xtj}{\hat x_{\tau,j}}
\newcommand{\xr}{\hat x_{\text{ridge}}}
\newcommand{\xml}{\hat x_{\text{ml}}}

\newcommand{\Htau}{H^*_\tau}

\DeclareMathOperator*{\argmax}{argmax}
\DeclareMathOperator*{\argmin}{argmin}
\DeclareMathOperator{\Res}{Res}
\DeclareMathOperator{\sgn}{sgn}
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\cov}{Cov}
\DeclareMathOperator{\erfc}{erfc}
\DeclareMathOperator{\erfcx}{erfcx}
\DeclareMathOperator{\tr}{tr}

\newtheorem{thm}{Theorem}
\newtheorem{prop}{Proposition}
\newtheorem{lem}{Lemma}
\newtheorem{cor}{Corollary}

\definecolor{uibr}{rgb}{0.859,0.247,0.239}
%\definecolor{uibgr}{rgb}{0.816,0.792,0.761}
\definecolor{uibgr}{rgb}{0.918,0.902,0.886}
\definecolor{uiby}{rgb}{0.804,0.671,0.247}
\definecolor{uibdb}{rgb}{0.102,0.149,0.251}
\definecolor{uibb}{rgb}{0.306,0.627,0.718}
\definecolor{uibg}{rgb}{0.471,0.604,0.357}
\definecolor{uibp}{rgb}{0.439,0.337,0.525}
\definecolor{uibo}{rgb}{0.925,0.412,0.082}

\newcommand{\strong}[1]{{\color{uibr}#1}}

\graphicspath{{./}{../../Logos/}{../../Figures/}}

\setlength{\parindent}{0pt}
\setlength{\parskip}{1ex plus 0.5ex minus 0.2ex}

\begin{document}

\pagecolor{uibgr}

\begin{tikzpicture}[x=\linewidth,y=.1\linewidth]
  \draw[fill=uibr,uibr] (0,0) rectangle (1,1);

  \node[align=left,text=white,font=\VeryHuge\bf,right] at (.05,.5) {Analytic solution and stationary phase approximation\\for the Bayesian lasso and elastic net}; 

  \node[align=right,text=white,font=\huge\bf,left] at (.95,.3) {Tom Michoel};

  % \node[align=right,text=white,font=\LARGE\bf,left] at (.97,.12)
  % {Computational Biology Unit, Department of Informatics, University of Bergen};  
  \node[align=right,text=white,font=\Large\bf,left] at (.95,.12)
  {\href{mailto:tom.michoel@uib.no}{tom.michoel@uib.no} $\mid$ \href{http://lab.michoel.info}{lab.michoel.info}};  

  \draw[fill=white,white] (0,-.06) rectangle (1,0);
  \draw[fill=uibdb,uibdb] (0.51,-0.04) rectangle (0.58,-0.02);
  \draw[fill=uibb,uibb] (0.58,-0.04) rectangle (0.65,-0.02);
  \draw[fill=uiby,uiby] (0.65,-0.04) rectangle (0.72,-0.02);
  \draw[fill=uibg,uibg] (0.72,-0.04) rectangle (0.79,-0.02);
  \draw[fill=uibr,uibr] (0.79,-0.04) rectangle (0.86,-0.02);
  \draw[fill=uibp,uibp] (0.86,-0.04) rectangle (0.93,-0.02);
  \draw[fill=uibo,uibo] (0.93,-0.04) rectangle (1.0,-0.02);
  
\end{tikzpicture}
%\vspace*{-20.8mm}

\begin{center}
  \begin{minipage}[t]{.22\linewidth}
    \begin{center}
      {\Large\bf Abstract}
    \end{center}
    \begin{itemize}
    \item The lasso and elastic net linear regression models impose
      a \textbf{double-exponential prior distribution} on the model
      parameters to achieve regression shrinkage and variable
      selection.
    \item There has been limited success in deriving estimates for
      the posterior distribution of regression coefficients in
      these models, due to a need to evaluate \textbf{analytically
        intractable partition function integrals}, not amenable to a
      convential Laplace approximation.
    \item We used the \textbf{Fourier transform} to express these
      integrals as complex-valued oscillatory integrals over
      ``regression frequencies'', which are amenable to a Gaussian
      approximation.
    \item An \textbf{analytic expansion} and \textbf{stationary
        phase approximation} in Fourier space is derived for the
      partition functions of the Bayesian lasso and elastic net.
    \item This approximation results in \textbf{highly accurate numerical
        estimates} at much \textbf{reduced computational cost}
      compared to Gibbs sampling. 
    \end{itemize}
    
    \vspace*{13mm}

    \begin{center}
      {\Large\bf  Bayesian elastic net model}
    \end{center}
    Response data $y\in\R^n$, predictor data $A\in\R^{n\times p}$.\\
    Regression coefficients $x\in\R^p$.
    
    Hierarchical model with double-exponential ($\ell_1$) prior:
    \begin{align*}
        p(y\mid A,x) &= \N(Ax,\sigma^2\U) \propto e^{-\frac1{2\sigma^2} \|y-Ax\|^2}\\[4mm] 
         p(x) &\propto e^{-\frac{n}{\sigma^2}\bigl( \lambda \|x\|^2 + 2\mu \|x\|_1 \bigr)}
      \end{align*}

      Posterior distribution of $x$:
      \begin{align*}
        p(x\mid y,A) \propto p(y\mid x,A)\; p(x) \propto e^{-\frac{n}{\sigma^2} \L(x\mid y,A)},
      \end{align*}
      where
      \begin{align*}
        \L (x\mid y,A) &= x^T \bigl(\frac{A^TA}{2n} + \lambda\U\bigr)x -2  \bigl(\frac{A^Ty}{2n}\bigr)^Tx + 2\mu \|x\|_1+ \frac1{2n}\|y\|^2
      \end{align*}
      is minus the posterior log-likelihood function.
    
    \vspace*{15mm}

    \begin{center}
      {\Large\bf  Problem formulation}
    \end{center}
    The Bayesian elastic net belongs to a more general class of models
    with cost functions
      \begin{align*}
        H(x\mid C,w,\mu) = x^TCx - 2w^Tx +2\mu \|x\|_1,
      \end{align*}
      where $C\in\R^{p\times p}$ is positive definite, $w\in\R^p$ and
      $\mu>0$. \\

      These cost/energy functions define Gibbs distributions
      \begin{align*}
        p(x\mid C,w,\mu,\tau) = \frac{e^{-\tau H(x\mid C,w,\mu)}}{Z(C,w,\mu,\tau)}.
      \end{align*}

      We seek to compute the \textbf{partition function}
      \begin{align*}
        Z(C,w,\mu,\tau) = \int_{\R^p} e^{-\tau H(x\mid C,w,\mu)} dx
      \end{align*}
      when the inverse temperature $\tau$ is large, but \textbf{cannot apply a Laplace approximation} because $H$ is not twice differentiable.
 
  \end{minipage}\qquad\vline\qquad
  \begin{minipage}[t]{.22\linewidth}
       \begin{center}
        {\Large\bf Key idea}
      \end{center}
      \begin{center}
        \begin{tabular}{ccc}
          \toprule
          \textbf{Maximum-likelihood} & $\mathbf{\Rightarrow}$ & \textbf{Bayesian inference}\\
          \midrule
          Legendre transform & $\Rightarrow$& Fourier transform\\
          Fenchel's duality theorem & $\Rightarrow$ & Parseval's identity\\
          \bottomrule
        \end{tabular}
        
        % \begin{minipage}[t]{.44\linewidth}
        %   \begin{flushright}
        %     \textbf{Convex dual} of log-likelihood  
        %   \end{flushright}
        % \end{minipage}
        % $\Leftrightarrow$
        % \begin{minipage}[t]{.45\linewidth}
        %   \begin{flushleft}
        %     \textbf{Fourier transform} of Gibbs distribution  
        %   \end{flushleft}
        % \end{minipage}

        % \bigskip

      %   \begin{minipage}{.44\linewidth}
      %     \begin{flushright}
      %       \textbf{Fenchel's duality theorem}
      %     \end{flushright}
      %   \end{minipage}
      %   $\Leftrightarrow$
      %   \begin{minipage}{.45\linewidth}
      %     \begin{flushleft}
      %       \textbf{Parseval's identity} 
      %     \end{flushleft}
      %   \end{minipage}
      \end{center}

      \bigskip

      Hence, write $f(x) = \frac12 x^TCx-w^Tx$,
      $g(x)=\mu\|x\|_1=\mu\sum_{j=1}^p |x_j|$ and use Parseval's identity
      to write the partition function as a $p$-dimensional
        \textbf{complex contour integral} in Fourier space:
      \begin{multline}\label{eq:1}
  Z = \int_{\R^p} e^{-2\tau f(x)} e^{-2\tau g(x)} dx = \int_{\R^p} \overline{\F(e^{-\tau f})}(k) \F(e^{-\tau g})(k) dk\\
        = \frac{(-i\mu)^p}{(\pi\tau)^{\frac{p}2}\sqrt{\det(C)}} 
      \int_{i\R^p} e^{\tau(z-w)^TC^{-1} (z-w)}
      \prod_{j=1}^p \frac{1}{\mu^2-z_j^2}\, dz
    \end{multline}
    The exponential factor in eq.~(\ref{eq:1}) has a saddle point at
      $z=w$, but poles at $z_j=\pm\mu$ prevent application of the
      standard stationary phase approximation (Fig.~1). We therefore write $Z$ as
      \begin{equation}\label{eq:2}
        Z = \frac{(-i\mu)^p}{(\pi\tau)^{\frac{p}2}\sqrt{\det(C)}} 
        \int_{i\R^p} e^{\tau  \Htau(z)} dz 
      \end{equation}
      and expand around the saddle point of 
      \begin{equation}\label{eq:htau}
        \Htau(z) = (z-w)^TC^{-1} (z-w) -\frac1\tau\sum_{j=1}^p  \ln(\mu^2-z_j^2)
      \end{equation}
   
%      \vspace*{15mm}

      \begin{center}
        \includegraphics[width=\linewidth]{fig_theory_surf}  
      \end{center}
      \textbf{Fig.~1}: Illustration in 1d: The quadratic exponent
      has a saddle point at $z=w$ but the integration contour cannot
      be deformed to pass through it if $|w|\geq\mu$  (a). By adding
      the logarithmic barrier function to the exponent, there is a
      unique saddle point $\ut$ with $|\ut|\leq\mu$ regardless of
      $|w|<\mu$ (b) or not (c).
      
    \vspace*{13mm}

      \begin{center}
        {\Large\bf The saddle point equations}
      \end{center}
     

      $\Htau$ has a unique saddle point $\ut$ in the domain
      $\D=\{z\in\C^p\colon |\Re z_j|<\mu,\; j=1,\dots,p\}$. $\ut$  is real, 
     $\ut\in \D\cap \R^p$, and solves the set of third order equations
     \begin{equation}\label{eq:03}
       (\mu^2-u_j^2)[C^{-1}(w-u)]_j - \frac{u_j}{\tau} = 0,\;  j\in\{1,\dots,p\}.
     \end{equation}

      As $\tau\to\infty$, $\uh=\lim_{\tau\to\infty}\ut$ is a solution to the set of equations
     \begin{align*}
       (u_j-\mu)(u_j+\mu)[C^{-1}(w-u)]_j=0\; \text{ subject to }\; |u_j|\leq\mu
     \end{align*}
     These are the optimality conditions for the convex dual problem of maximizing the elastic net log-likelihood. The maximum-likelihood coefficients $\xh$ satisfy $\xh=C^{-1}(w-\uh)=\lim_{\tau\to\infty}\xt$ where $\xt=C^{-1}(w-\ut)$, and $\xh_j\neq0 \Leftrightarrow \uh_j=\pm\mu$.

      
 \end{minipage}\qquad\vline\qquad
\begin{minipage}[t]{.44\linewidth}
  \begin{center}
    {\Large\bf The stationary phase approximation}
  \end{center}
  
  \vspace*{4mm}

  \begin{minipage}[t]{.485\linewidth}
    By shifting the integration contour in eq.~(\ref{eq:2}) to a
    \textbf{steepest descent contour} passing through the saddle point
    (Fig.~1), a \textbf{Gaussian approximation} to the partition
    function is obtained:
      \begin{equation}\label{eq:Z-approx}
        Z \sim \Bigl(\frac{\mu}{\sqrt\tau}\Bigr)^p e^{\tau (w-\ut )^T C^{-1}
          (w-\ut)} \prod_{j=1}^p\frac{1}{\sqrt{\mu^2+\utj^2}}
        \frac{1}{\sqrt{\det(C+D_\tau)}},
      \end{equation}
      where $D_\tau$ is a diagonal matrix with diagonal elements $\frac{\tau(\mu^2 - \utj^2)^2}{\mu^2+\utj^2}$.

      \medskip

      Although $\Htau$ and the saddle point depend on $\tau$, standard stationary phase approximation estimates still hold and allow to prove that this is a bona fide  \textbf{analytic approximation} (i.e. higher-order terms vanish for large $\tau$ relative to the quadratic term).
    \end{minipage}\hfill
    \begin{minipage}[t]{.485\linewidth}
      The stationary phase approximation is used to compute:
      \begin{itemize}
      \item \textbf{Posterior expectation values}:
        \begin{align*}
          \E_\tau(x) \sim\xt = C^{-1}(w-\ut)
        \end{align*}
      \item \textbf{Marginal posterior distributions}:
        \begin{align*}
          p(x_j) &= e^{-\tau(C_{jj}x_j^2 - 2w_j x_j + 2\mu |x_j|)} 
                   \frac{Z(C_{\setminus j},w_{\setminus j}-x_jC_{\setminus
                   j,j},\mu)}{Z}
        \end{align*}
      \item \textbf{Posterior predictive distributions}:
        \begin{align*}%\label{eq:3}
          p(y) &=  \Bigl(\frac{\tau}{2\pi n}\Bigr)^{\frac 12} e^{-\frac\tau{2n}y^2} \frac{Z(C+\frac1{2n}aa^T,w+\frac{y}{2n}a,\mu)}{Z}
        \end{align*}
      \end{itemize}
    \end{minipage}

    \vspace*{15mm}
    
    \begin{minipage}{\linewidth}
      \begin{center}
        {\Large\bf Numerical results}
      \end{center}
      
      \vspace*{4mm}

    \begin{minipage}[t]{.485\linewidth}
      \textbf{Marginal posterior distributions} using
      eq.~(\ref{eq:Z-approx}) are indistinguishable from those
      obtained by Gibbs sampling, but not if we approximate $\ut$ by
      its maximum-likelihood limit $\uh$ (Fig.~2).

        \begin{center}
          \includegraphics[width=.8\linewidth]{fig_marginal_distributions}  
        \end{center}
        \textbf{Fig.~2}: Marginal posterior distributions for the
        diabetes (top) and leukemia (bottom) data for  a zero,
        transition and non-zero maximum-likelihood predictor (left to
        right). Blue, Gibbs sampling; red, stationary phase
        approximation; yellow, maximum-likelihood-based approximation.
    \end{minipage}
    \hfill
    \begin{minipage}[t]{.485\linewidth}
      \textbf{Predictive accuracy} of the stationary phase approximation is
      comparable to state-of-the-art Gibbs sampling implementations
      for the Bayesian lasso and horseshoe (BayReg package)  (Fig.~3).\\
      
      \smallskip

      \begin{center}
        \includegraphics[width=\linewidth]{ccle_cv_alldrugs} 
      \end{center}
      \smallskip

      \textbf{Fig~3}: Median accuracy for predicting dug sensitivity
      from gene expression in the Cancer Cell Line Encyclopedia using
      10$\times$ cross-validation, using the analytic approximation
      for the Bayesian elastic net (red), BayReg's Bayesian lasso
      (blue) and horseshoe (yellow), and maximum-likelihood elastic
      net (purple) and ridge regression (green).
    \end{minipage}
  \end{minipage}

  \vspace*{15mm}

  \begin{minipage}[t]{.485\linewidth}
    \begin{center}
      {\Large\bf FAQ}
    \end{center}
    \begin{itemize}
    \item \textbf{Why Bayesian lasso/elastic net? Expectation values
        for the coefficients are not sparse!}\\
      Bayesian lasso/elastic net is suitable to model
      \textbf{heavy-tail} effect sizes. This is more realistic for
      many natural systems (e.g.\ genetic association studies) than
      normally distributed or strictly sparse effect sizes.
    \item \textbf{Can results be extended to logistic regression or
        other generalized linear models?}\\
      The non-differentiable double-exponential prior transforms to a
      well-behaved exponential of a log-barrier function in
      \textbf{all} $\ell_1$-penalized GLMs. In eqs.~\eqref{eq:03}, $C$ will then be replaced by the
      Hessian of the unpenalized model evaluated at the saddle point
      itself, making the numerics more complicated.
    \item \textbf{How to determine the inverse temperature and why
        can it be assumed large?}\\
      The inverse temperature $\tau=n/\sigma^2$ is large because we
      assume large sample size $n$ and small residual variance
      $\sigma^2$. In our experiments, we determined
      $\hat\tau$ by its MAP value or by cross-validation.
    \end{itemize}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{.485\linewidth}
    \begin{center}
      {\Large\bf Conclusions}
    \end{center}
    \begin{itemize}
    \item Expressing intractable partition function integrals as
      complex-valued oscillatory integrals through the \textbf{Fourier
      transform} is a powerful approach for performing Bayesian
      inference in $\ell_1$-penalized models.
    \item Use of the \textbf{stationary phase approximation} to these
      integrals results in highly accurate estimates for the posterior
      expectation values, marginal posterior distributions, and
      posterior predictive distributions at a much reduced
      computational cost compared to Gibbs sampling.
    \item The analytical methods are generic and show that
      \textbf{powerful duality principles exist to study Bayesian
        inference problems}. These are generalizations to finite
      inverse temperature of the convex duality principles that have
      been essential to characterize analytical properties of the
      max\-imum-like\-li\-hood solutions of $\ell_1$-penalized and
      other regression models.
    \end{itemize}
  \end{minipage}
\end{minipage}
\end{center}

\vfill
%\vspace*{-4mm}
\colorbox{white}{
\hspace*{33mm}
\begin{minipage}{.67\linewidth}
  \begin{minipage}[t]{.3\linewidth}
    {\Large\bf Acknowledgments}\\[6mm]
    This work was supported by grants from the BBSRC (B/J004235/1, BB/M020053/1) while the author was affiliated with the University of Edinburgh (2012--2018).\\
  \end{minipage}\qquad\quad %\hspace*{.02\linewidth}
  \begin{minipage}[t]{.25\linewidth}
    {\Large\bf Paper}\\[2mm]
    { Available at \href{https://arxiv.org/abs/1709.08535}{\bf arxiv.org/abs/1709.08535}}
    
    \bigskip
    
    {\Large\bf Code}\\[2mm]
    { Available at \href{https://github.com/tmichoel/bayonet}{\bf github.com/tmichoel/bayonet}}
    
    % \vspace*{9mm}
  \end{minipage} %\hspace*{.02\linewidth}
  \begin{minipage}[t]{.26\linewidth}
    {\Large\bf AI/ML positions available}\\[2mm]
    UiB is expanding research in AI/ML and recruiting at all levels. See \href{http://www.uib.no/en/ii}{\bf uib.no/en/ii}.
  \end{minipage}
\end{minipage}\hfill
\begin{minipage}{.3\linewidth}
  \includegraphics[width=.95\linewidth]{cbu_inf_uib_logo}%\\%[2mm]
\end{minipage}}


\end{document}
